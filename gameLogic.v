

module gameLogic(clock, resetn, 
					distance, 
					go, 
					entity, mode, firePos, fireState, plot, done, x,
					score);
	input clock, resetn;

	output [7:0] score;
	
	//distance input from sensor (raw data)
	input [8:0] distance;
	
	//go input from user
	input go;
	
	//x, y position outputs to graphic logic
	output [7:0] x;
	
	output entity, mode, plot;
	output [1:0] fireState, firePos;

	input done;

	
	wire ldDistance, advanceFire, selFirePos, collision, gameTime, gameTick, resetDuration, ldTime, increaseSpeed, countScore, resetScore, ldPlayerTime, timePlayer, playerTick;
	
	wire [1:0] fireStateTemp;
	
	controlPathLogic c(
		.clock(clock),
		.resetn(resetn),
		
		.go(go),
		
		//output to graphics logic
		.mode(mode),
		.entity(entity),
		.plot(plot),
		
		//input from graphics logic
		.done(done),
		
		//control signals to datapath
		.ldDistance(ldDistance),
		.selFirePos(selFirePos),
		.advanceFire(advanceFire),
		.gameTime(gameTime),
		.gameTick(gameTick),
		.resetDuration(resetDuration),
		.ldTime(ldTime),
		.increaseSpeed(increaseSpeed),
		.countScore(countScore),
		.resetScore(resetScore),
		.ldPlayerTime(ldPlayerTime),
		.timePlayer(timePlayer),
		
		
		//data signals from datapath
		.fireState(fireStateTemp),
		.collision(collision),
		.playerTick(playerTick)
	);

	datapathLogic d(
		.clock(clock),
		.resetn(resetn),
		
		.distance(distance),
		.x(x),
		
		.firePos(firePos),
		
		.score(score),
		
		//control signals to datapath
		.ldDistance(ldDistance),
		.selFirePos(selFirePos),
		.advanceFire(advanceFire),
		.gameTime(gameTime),
		.gameTick(gameTick),
		.resetDuration(resetDuration),
		.ldTime(ldTime),
		.increaseSpeed(increaseSpeed),
		.countScore(countScore),
		.resetScore(resetScore),
		.ldPlayerTime(ldPlayerTime),
		.timePlayer(timePlayer),
		
		
		//data signals from datapath
		.fireState(fireStateTemp),
		.collision(collision),
		.playerTick(playerTick)
	
	
	);

	assign fireState = fireStateTemp;

endmodule


module controlPathLogic(clock, resetn, 
					go, 
					mode, entity, plot, done,

					ldDistance, selFirePos, advanceFire, gameTime, gameTick, ldTime, resetDuration, increaseSpeed, fireState, collision, resetScore, countScore, ldPlayerTime, timePlayer, playerTick);
	input clock, resetn;
	
	//go from user
	input go;
	

	//graphics logic i/o
	output reg mode, entity, plot;
	input done;

	//control signals
	output reg ldDistance, advanceFire, selFirePos, gameTime, resetDuration, ldTime, increaseSpeed, resetScore, countScore, ldPlayerTime, timePlayer;
	input gameTick, collision, playerTick;
	input [1:0] fireState;

	
	reg [4:0] currentState, nextState;
	
	
	parameter [4:0]             NEWGAME =        5'd0,
								NEWGAMEWAIT =    5'd1,
								
								ERASEPLAYER =    5'd2,
								WAITP0 =         5'd3,
								NEWPLAYERX =     5'd4,
								DRAWPLAYER =     5'd5,
								WAITP1 =         5'd6,
								
								FIREPOS =        5'd7,
								DRAWFIRE =       5'd8,
								WAITD =          5'd9,
								ADVANCE =        5'd10,
								ERASEBIG =       5'd11,
								WAITE =          5'd12,
								CHECKCOLLISION = 5'd13,
								SPEEDUP =        5'd14,
								ADVANCEFIRE =	 5'd15,
								PLAYERWAIT = 	 5'd16;
								
								
								
	//state transitions
	always @(*)
	begin: stateTransitions
		case(currentState)
			NEWGAME: 		nextState = go ? NEWGAMEWAIT : NEWGAME;
			NEWGAMEWAIT: 	nextState = go ? NEWGAMEWAIT : FIREPOS;
			FIREPOS: 		nextState = DRAWFIRE;
			DRAWFIRE:		nextState = WAITD;
			WAITD: 			nextState = done ? ERASEPLAYER : WAITD;
			ERASEPLAYER: 	nextState = WAITP0;
			WAITP0:  		nextState = done ? NEWPLAYERX : WAITP0;
			NEWPLAYERX:		nextState = DRAWPLAYER;
			DRAWPLAYER: 	nextState = WAITP1;
			WAITP1:			nextState = done ? PLAYERWAIT : WAITP1;
			PLAYERWAIT:		nextState = playerTick ? ( gameTick ? ADVANCE : ERASEPLAYER ): PLAYERWAIT;
			ADVANCE: 		nextState = (fireState == 2'd2) ? ERASEBIG : ADVANCEFIRE;
			ADVANCEFIRE:	nextState = DRAWFIRE;
			ERASEBIG:		nextState = WAITE;
			WAITE: 			nextState = done ? CHECKCOLLISION : WAITE;
			CHECKCOLLISION: nextState = collision ? NEWGAME : SPEEDUP;
			SPEEDUP: 		nextState = FIREPOS;
		endcase
	end
	
	
	//control signal logic

	always @(*)
	begin: controlLogic
		selFirePos = 0;
		ldDistance = 0;
		advanceFire = 0;
		gameTime = 0;
		ldTime = 0;
		increaseSpeed = 0;
		countScore = 0;
		resetScore = 0;
		resetDuration = 0;
		timePlayer = 0;
		ldPlayerTime = 0;
		
		mode = 0; 
		entity = 0;
		plot = 0;
		case(currentState)
			NEWGAMEWAIT: begin
				resetScore = 1;
				resetDuration = 1;
				end
			FIREPOS: begin
				//sets the datapath to select a new position for the fire
				selFirePos = 1;
			end
			DRAWFIRE: begin
				//mode = 0 -> draw mode
				//entity = 0 -> draw fire
				plot = 1;
				ldTime = 1;
			end
			
			//loop for when drawing player cusor and waiting for game to advance
			ERASEPLAYER: begin
				mode = 1;
				entity = 1;
				plot = 1;
				
				gameTime = 1;
			end
			WAITP0: begin
				gameTime = 1;
				
				entity = 1;
				mode = 1;
			end
			NEWPLAYERX: begin
				ldDistance = 1;
				
				gameTime = 1;
			end
			DRAWPLAYER: begin
				//mode = 0 -> draw player
				entity = 1;
				plot = 1;
				
				gameTime = 1;
			end
			WAITP1: begin
				gameTime = 1;
				
				ldPlayerTime = 1;
				
				entity = 1;
				mode = 0;
			end
			
			PLAYERWAIT: begin
				gameTime = 1;
				timePlayer = 1;
			
			end
			
			//advance the game state
			ADVANCEFIRE: begin
				advanceFire = 1;
			
			end
			
			ERASEBIG: begin
				mode = 1;
				//entity = 0 -> fire entity is selected
				plot = 1;
				
				
				//need proper erase wait state
			end
			
			WAITE: begin
				mode = 1;
				//entity = 0 
	
			end
			
			CHECKCOLLISION: begin
				advanceFire = 1; //ensures that the fire state is reset back to 0
			
			end
			
			SPEEDUP: begin
				increaseSpeed = 1;
				countScore = 1;
			end
		endcase
	end
	
								

	//state flip flop 
	always @(posedge clock)
	begin: stateFFs
		if(!resetn)
			currentState = NEWGAME;
		else
			currentState = nextState;
	end

								
								
endmodule




module datapathLogic(clock, resetn, 
				distance, x,
				fireState,
				firePos,
				score,
				ldDistance, advanceFire, selFirePos, gameTime, gameTick, ldTime, increaseSpeed, collision, resetScore, countScore, resetDuration, ldPlayerTime, timePlayer, playerTick); 
				
				
				input clock, resetn;
				
				input [8:0] distance;
				output reg [7:0] x;
				
				
				output reg [1:0] fireState, firePos;
				output reg [7:0] score;
				
				input ldDistance, advanceFire, selFirePos, gameTime, resetDuration, ldTime, increaseSpeed, resetScore, countScore, ldPlayerTime, timePlayer;
				output gameTick, collision, playerTick;
				
				
				//collision logic
				
				parameter  [7:0]	FIREWIDTH 	= 40,
									FIRELEFT  	= 20,
									FIRECENTER	= 70,
									FIRERIGHT   = 115;
				
				reg [7:0] fireCurrent;
				
				always @(*)
				begin
					case(firePos)
						2'd0: fireCurrent = FIRELEFT;
						2'd1: fireCurrent = FIRECENTER;
						2'd2: fireCurrent = FIRERIGHT;
						default: fireCurrent = FIRELEFT;
					endcase
				end
				
				
				
				assign collision = ((x > fireCurrent) && (x < (fireCurrent + FIREWIDTH)));
				
				
				parameter [8:0]  	SHIFT = 9'd10,
									SCALE = 9'd3;
				
				///mapping the distance -> x
				always @(posedge clock)
				begin
					if(!resetn)
						x <= 0;
					else if(ldDistance)
						x <=  (distance - SHIFT)*SCALE [7:0] ;	
				end
				
				//determine the state of the fire ie close, med, far
				//counts through the states, 0 is far, 2 is about to hit player
				always @(posedge clock)
				begin
					if(!resetn)
						fireState <= 0;
					else if(advanceFire)
					begin
						if(fireState == 2)
							fireState <= 0;
						else
							fireState <= fireState + 1;
					end
				end
				
				//determine the position of the fire
				//just loops through left, center, right positions
				always @(posedge clock)
				begin
					if(!resetn)
						firePos = 0;
					else if(selFirePos)
					begin
						if(firePos == 2)
							firePos <= 0;
						else
							firePos <= firePos + 1;
					end
				end

				
				//decrement register
				reg [26:0] duration;
				
				parameter [26:0] 	decrement = 27'd10000000, 
									defaultLength = 27'd134217727;
									
				//parameter [26:0] 	decrement = 27'd1, defaultLength = 27'd4;					
				//decrement counter
				
				always @(posedge clock)
				begin
					if(!resetn || resetDuration)
						duration = defaultLength;
					else if(increaseSpeed)
						duration = duration - decrement;
				end
				
				countdownTimerLogic gameTimer(
					.clock(clock),
					.resetn(resetn),
					.length(duration),
					.load(ldTime),
					.engage(gameTime),
					.tick(gameTick)
				);
				
				countdownTimerLogic playerTimerLogic(
					.clock(clock),
					.resetn(resetn),
					.length(27'd10000),
					.load(ldPlayerTime),
					.engage(timePlayer),
					.tick(playerTick)
				);
				
				//score counter
				always @(posedge clock)
				begin
					if(!resetn || resetScore)
						score <= 8'd0;
					else if(countScore)
						score <= score + 8'd1;
				end
				

endmodule 


module countdownTimerLogic(clock, resetn, load, engage, length, tick);
	input clock, resetn, engage, load;
	input [26:0] length;
	
	output tick;

	reg [26:0] count;
	
	always @(posedge clock)
	begin
		if(!resetn || load == 1)
			count = length;
		else if(engage == 1'b1 && count != 0)
			count = count - 1;
	end
	
	assign tick = (count == 27'd0);
	
endmodule