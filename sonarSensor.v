


///////////////////////////////////////////////////////
//modules for sonar sensor

module sonarSensor(clock, resetn, trigger, echo, distance);
	input clock, resetn, echo;
	
	
	output trigger;
	output [8:0] distance;


	//control signals
	wire tick, measure, ldOut, trig;
	
	//control path
	controlPath c(
		.clock(clock),
		.resetn(resetn),
		
		//sensor control
		.trigger(trig),
		.echo(echo),
		
		//control signals
		.tick(tick),
		.measure(measure),
		.ldOut(ldOut)
	);
	
	
	
	//data path
	datapath d(
		.clock(clock),
		.resetn(resetn),
		
		.tick(tick),
		
		.trigger(trig),
		.measure(measure),
		.ldOut(ldOut),
		
		.distance(distance)
	);
	
	assign trigger = trig;
	
endmodule




module controlPath(clock, resetn, trigger, echo, tick, measure, ldOut);
	//primary signals
	input clock, resetn;
	
	//sensor control
	output reg trigger;
	input echo;
	
	//incoming control signals
	input tick;
	
	//outgoing control signals
	output reg measure, ldOut;

	reg [1:0] currentState, nextState;
	
	parameter [1:0] TRIGGER = 2'd0,
					WAIT    = 2'd1,
					MEASURE = 2'd2,
					OUTPUT  = 2'd3;
					
	
	//always block to determine next state
	always @(*)
	begin
		case(currentState)
			TRIGGER: nextState = tick ? WAIT : TRIGGER;
			WAIT:    nextState = echo ? MEASURE : WAIT;
			MEASURE: nextState = echo ? MEASURE : OUTPUT;
			OUTPUT:  nextState = TRIGGER;
			default: nextState = trigger;
		endcase
	end
		
	//control wire designations
	always @(*)
	begin: controlWires
		trigger = 1'b0;
		ldOut = 1'b0;
		measure = 1'b0;
		case(currentState)
			TRIGGER: trigger = 1'b1;
			MEASURE: measure = 1'b1;
			OUTPUT: ldOut    = 1'b1;
		endcase
	
	
	end
		
	//state registers
	always @(posedge clock)
	begin: stateFFs
		if(resetn == 1'b0)
			currentState = TRIGGER;
		else
			currentState = nextState;
	end
endmodule


module datapath(clock, resetn, tick, trigger, measure, ldOut, distance);

	input clock, resetn;
	
	output tick;
	
	input trigger, measure, ldOut;
	
	output reg [8:0] distance;
	reg [8:0] dReg;
	
	countdownTimer triggerTimer(
		.clock(clock),
		.resetn(resetn),
		.engage(trigger),
		//change to 500 for actual operation
		// 500 clock cycles = (500-1)*20ns = 10us (length of time required for trigger)
		.length(12'd499),
		.tick(tick)
	);
	
	wire count;
	
	countdownTimer measureTimer(
		.clock(clock),
		.resetn(resetn),
		.engage(measure),
		//change to 2900 for actual operation
		// 2900 clock cycles = (2900 - 1)*20ns = 58 us ( 1cm/58us from the input of the sonar sensor)
		.length(12'd2899),
		.tick(count)
	);
	
	//output and distance count registers
	always @(posedge clock)
	begin
		if(resetn == 1'b0)
		begin
			distance = 9'd0;
			dReg     = 9'd0;
		end
		else
		begin
			if(count)
				dReg = dReg + 9'd1;
			if(ldOut)
			begin
				distance = dReg;
				dReg = 9'd0;
			end
		end
	end
endmodule



module countdownTimer(clock, resetn, engage, length, tick);
	input clock, resetn, engage;
	input [11:0] length;
	
	output tick;

	reg [11:0] count;
	
	always @(posedge clock)
	begin
		if(resetn == 1'b0 || count == 12'b0)
			count = length;
		else if(engage == 1'b1)
			count = count - 1;
	end
	
	assign tick = (count == 12'd0);
	
endmodule

//end modules for sonar sensor
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////
//hexDecoder////////////////////////////////
//takes a 4 bit input, and assigns that to a 6 bit hex input for the board
//takes an arbitrary array which represents the 4 bits, and the 7 bits for the
//hex display
////////////////////////////////////////////
module hexDecoder(inp, out);

	input [3:0] inp;
	output [6:0] out;


	wire c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, ca, cb, cc, cd, ce, cf;
	
	
	
	assign c0 = ~inp[3] & ~inp[2] & ~inp[1] & ~inp[0];
	assign c1 = ~inp[3] & ~inp[2] & ~inp[1] & inp[0];
	assign c2 = ~inp[3] & ~inp[2] & inp[1] & ~inp[0];
	assign c3 = ~inp[3] & ~inp[2] & inp[1] & inp[0];
	
	assign c4 = ~inp[3] & inp[2] & ~inp[1] & ~inp[0];
	assign c5 = ~inp[3] & inp[2] & ~inp[1] & inp[0];
	assign c6 = ~inp[3] & inp[2] & inp[1] & ~inp[0];
	assign c7 = ~inp[3] & inp[2] & inp[1] & inp[0];
	
	assign c8 = inp[3] & ~inp[2] & ~inp[1] & ~inp[0];
	assign c9 = inp[3] & ~inp[2] & ~inp[1] & inp[0];
	assign ca = inp[3] & ~inp[2] & inp[1] & ~inp[0];
	assign cb = inp[3] & ~inp[2] & inp[1] & inp[0];
	
	assign cc = inp[3] & inp[2] & ~inp[1] & ~inp[0];
	assign cd = inp[3] & inp[2] & ~inp[1] & inp[0];
	assign ce = inp[3] & inp[2] & inp[1] & ~inp[0];
	assign cf = inp[3] & inp[2] & inp[1] & inp[0];
	
	
	//true if the display should not be on for the specific number
	//eg for 0, HEX6 is not on, so if the number is 0, that display will be high (Off state)
	assign out[0] = (c1 + c4 + c6 + cb + cd);
	assign out[1] = (c5 + c6 + cb + cc + ce + cf);
	assign out[2] = (c2 + cc + cf + ce);
	assign out[3] = (c1 + c4 + c7 + ca + cf);
	assign out[4] = (c1 + c3 + c4 + c5 + c7 + c9);
	assign out[5] = (c1 + c2 + c3 + c7 + cd);
	assign out[6] = (c0 + c1 + c7 + cc); 

endmodule 

