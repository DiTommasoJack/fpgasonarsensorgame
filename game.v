

module game(clock, resetn, go, trigger, echo, score,
			writeEn, col, x, y);
	input clock, resetn;
	
	//VGA outputs
	output writeEn;
	output [5:0] col;
	output [7:0] x;
	output [6:0] y;
	
	
	//go signal from user
	//game will not go until go is high
	input go;

	
	//trigger -> trigger signal to distance sensor
	//echo -> measure signal from sonar sensor
	output trigger;
	input echo;

	//score output to user
	output [7:0] score;


	
	
	wire [8:0] distance;
	
	wire entity, mode, plot, done;
	wire [1:0] firePos, fireState;
	wire [7:0] xIn;
	
	part2 p0 (
		.clock(clock),
		.resetn(resetn),
		.pos(firePos),
		.go(plot),
		.col(col),
		.x(x),
		.y(y),
		.enable(writeEn),
		.entity(entity),
		.draw_mode(mode),
		.selSize(fireState),
		.xIn(xIn),
		.done(done)
	);

	gameLogic logicUnit(
		.clock(clock), 
		.resetn(resetn),

		//distance from the sonar sensor
		.distance(distance),
		
		//go signal from user
		.go(go),
		
	
		//outputs to graphic controller
		.entity(entity),
		.mode(mode),
		.plot(plot),
		.firePos(firePos),
		.fireState(fireState),
		.x(xIn),
		
		//done signal from gaphics logic
		.done(done),
		
		//score output
		.score(score)
		);

	sonarSensor sensor(
		.clock(clock),
		.resetn(resetn),
		.trigger(trigger),
		.echo(echo),
		.distance(distance)
	);

endmodule 