module main(

		CLOCK_50,
		KEY,
		GPIO_0,
		HEX0,
		HEX1,

		// The ports below are for the VGA output.  Do not change.
		VGA_CLK,   						//	VGA Clock
		VGA_HS,							//	VGA H_SYNC
		VGA_VS,							//	VGA V_SYNC
		VGA_BLANK_N,						//	VGA BLANK
		VGA_SYNC_N,						//	VGA SYNC
		VGA_R,   						//	VGA Red[9:0]
		VGA_G,	 						//	VGA Green[9:0]
		VGA_B   						//	VGA Blue[9:0]

);
	input CLOCK_50;
	input [3:0] KEY;
	inout [2:0] GPIO_0;
	
	
	output [6:0] HEX0;
	output [6:0] HEX1;

	output			VGA_CLK;   				//	VGA Clock
	output			VGA_HS;					//	VGA H_SYNC
	output			VGA_VS;					//	VGA V_SYNC
	output			VGA_BLANK_N;				//	VGA BLANK
	output			VGA_SYNC_N;				//	VGA SYNC
	output	[7:0]	VGA_R;   				//	VGA Red[9:0]
	output	[7:0]	VGA_G;	 				//	VGA Green[9:0]
	output	[7:0]	VGA_B;   				//	VGA Blue[9:0]
	
	wire resetn;
	wire [7:0] x;
	wire [6:0] y;
	wire [5:0] col;
	wire writeEn;
	
	wire [7:0] score;
	
	assign resetn = KEY[0];
	
	// Create an Instance of a VGA controller - there can be only one!
	// sim:/part2Define the number of colours as well as the initial background
	// image file (.MIF) for the controller.
	vga_adapter VGA(
			.resetn(resetn),
			.clock(CLOCK_50),
			.plot(writeEn),
			.colour(col),
			.x(x),
			.y(y),
			/* Signals for the DAC to drive the monitor. */
			.VGA_R(VGA_R),
			.VGA_G(VGA_G),
			.VGA_B(VGA_B),
			.VGA_HS(VGA_HS),
			.VGA_VS(VGA_VS),
			.VGA_BLANK(VGA_BLANK_N),
			.VGA_SYNC(VGA_SYNC_N),
			.VGA_CLK(VGA_CLK));
		defparam VGA.RESOLUTION = "160x120";
		defparam VGA.MONOCHROME = "FALSE";
		defparam VGA.BITS_PER_COLOUR_CHANNEL = 2;
		defparam VGA.BACKGROUND_IMAGE = "background1.mif";
			
	// Put your code here. 
	// for the VGA controller, in addition to any other functionality your design may require.

	game gameInstance(
		.clock(CLOCK_50),
		.resetn(resetn),
		.go(!KEY[1]),
		
		//signals for distance sensor
		.trigger(GPIO_0[0]),
		.echo(GPIO_0[2]),
		
		//score signals
		.score(score),
		
		//VGA control signals
		.writeEn(writeEn),
		.col(col),
		.x(x),
		.y(y)
	);
	
	
	hex_decoder h0 (.hex_digit(score[3:0]),
						 .segments(HEX0)
						 );
						 
	hex_decoder h1 (.hex_digit(score[7:4]),
						 .segments(HEX1)
						 );
	
endmodule

module hex_decoder(hex_digit, segments);
    input [3:0] hex_digit;
    output reg [6:0] segments;
   
    always @(*)
        case (hex_digit)
            4'h0: segments = 7'b100_0000;
            4'h1: segments = 7'b111_1001;
            4'h2: segments = 7'b010_0100;
            4'h3: segments = 7'b011_0000;
            4'h4: segments = 7'b001_1001;
            4'h5: segments = 7'b001_0010;
            4'h6: segments = 7'b000_0010;
            4'h7: segments = 7'b111_1000;
            4'h8: segments = 7'b000_0000;
            4'h9: segments = 7'b001_1000;
            4'hA: segments = 7'b000_1000;
            4'hB: segments = 7'b000_0011;
            4'hC: segments = 7'b100_0110;
            4'hD: segments = 7'b010_0001;
            4'hE: segments = 7'b000_0110;
            4'hF: segments = 7'b000_1110;   
            default: segments = 7'h7f;
        endcase
endmodule



