
//SRVD/Homes$/singhr88/Downloads/ECE241_Labs/Project/fill.v
module part2(
    input resetn,
    input go,
    input clock,
	 input draw_mode,
	 input entity,
	 input [7:0] xIn,
	 input [1:0] selSize,
    input [1:0] pos,
    output enable,
    output [5:0] col,
    output [7:0] x,
    output [6:0] y,
	 output done
    );
    wire writeEnable;
	wire [7:0] countX;
	wire [6:0] countY;
	wire [5:0] colData; 
	wire ld_x, ld_col, ld_y, set_x, set_y;
	wire doneCount;
	wire increment;

	controlFill c0 (
		.clk(clock),
		.resetn(resetn),
		.go(go),
		.done(done),
		.doneCount(doneCount),
		.ld_y(ld_y), 
		.ld_x(ld_x),
		.ld_col(ld_col),
		.set_x(set_x),
		.set_y(set_y),
		.enable(writeEnable),
		.increment(increment)
    );
	
	datapathFill d0 (
		.clk(clock),
		.resetn(resetn),
		.selectPos(pos),
		.selSize(selSize),
		.xIn(xIn),
		.increment(increment),
		.ld_x(ld_x),
		.ld_y(ld_y),
		.ld_col(ld_col),
		.outX(countX),
		.outY(countY),
		.outCol(colData),
		.set_x(set_x),
		.set_y(set_y),
		.draw_mode(draw_mode),
		.entity(entity),
		.done(doneCount)
    );

	assign x = countX;
	assign y = countY;
	assign col = colData;
	assign enable = writeEnable;
    
 endmodule        
                

module controlFill(
    input clk,
    input resetn,
    input go,
	 input doneCount,
    output reg enable,
	 output reg done,
    output reg ld_x, ld_y, ld_col, set_x, set_y, increment
    );

    reg [3:0] current_state, next_state; 
    
    localparam 
		S_DONE = 3'd0,
		S_SET_XY = 3'd1,
		S_LOAD_XY    = 3'd2,
		S_LOAD_COL = 3'd3,
		S_PLOT    = 3'd4;
    
    // Next state logic aka our state table
    always@(*)
    begin: state_table 
            case (current_state)
		S_DONE: next_state = go ? S_SET_XY : S_DONE;
		S_SET_XY: next_state = S_LOAD_XY;
		S_LOAD_XY: next_state = S_LOAD_COL;
		S_LOAD_COL: next_state = S_PLOT;
      S_PLOT:  begin 
				if (doneCount)
					next_state = S_DONE;
				else 
					next_state = S_LOAD_XY;
			end 
            default:     next_state = S_DONE;
        endcase
    end // state_table
   

    // Output logic aka all of our datapath control signals
    // enable = write enable
    // ld_x = load x value from register
    // ld_y = load y value from register
    // set_x = set x count
    // set_y = set y count
    always @(*)
    begin: enable_signals
        // By default make all our signals 0
		enable = 1'b0;
		ld_x = 1'b0;
		ld_y = 1'b0;
		ld_col = 1'b0;
		set_x = 1'b0;
		set_y = 1'b0;
		done = 1'b0;
		increment = 1'b0;

        case (current_state)
		  	   S_DONE: begin
		enable = 1'b0;
		ld_x = 1'b0;
		ld_y = 1'b0;
		ld_col = 1'b0;
		set_x = 1'b0;
		set_y = 1'b0;
		done = 1'b1;
		increment = 1'b0;
                end
            S_SET_XY: begin
		enable = 1'b0;
		ld_x = 1'b0;
		ld_y = 1'b0;
		ld_col = 1'b0;
		set_x = 1'b1;
		set_y = 1'b1;
		done = 1'b0;
		increment = 1'b0;
                end
				S_LOAD_XY: begin
		enable = 1'b0;
		ld_x = 1'b1;
		ld_y = 1'b1;
		ld_col = 1'b0;
		set_x = 1'b0;
		set_y = 1'b0;
		done = 1'b0;
		increment = 1'b0;
					 end
            S_LOAD_COL: begin
		enable = 1'b0;
		ld_x = 1'b0;
		ld_y = 1'b0;
		ld_col = 1'b1;
		set_x = 1'b0;
		set_y = 1'b0;
		done = 1'b0;
		increment = 1'b1;
                end
            S_PLOT: begin
		enable = 1'b1;
		ld_x = 1'b0;
		ld_y = 1'b0;
		ld_col = 1'b0;
		set_x = 1'b0;
		set_y = 1'b0;
		done = 1'b0;
		increment = 1'b0;
                end
        // default:    // don't need default since we already made sure all of our outputs were assigned a value at the start of the always block
        endcase
    end // enable_signals
   
    // current_state registers
    always@(posedge clk)
    begin: state_FFs
        if(!resetn)
            current_state <= S_DONE;
        else
            current_state <= next_state;
    end // state_FFS
endmodule

module datapathFill(
    input clk,
    input resetn,
    input increment,
    input [1:0] selectPos,
	 input [1:0] selSize,
	 input draw_mode,
	 input entity,
	 input [7:0] xIn,
    input ld_x, ld_y, ld_col, set_x, set_y,
    output reg  [7:0] outX,
    output reg [6:0] outY, 
    output reg [5:0] outCol,
	 output reg done
    );
    // Input Registers
    reg [7:0] countX;
    reg [6:0] countY;
    reg [5:0] mem_out;
	 reg [5:0] fire_out;
	 reg [7:0] xOffset;
	 reg [7:0] xBound;
	 reg [6:0] yOffset;
	 reg [6:0] yBound;
	 wire [5:0] big_out;
	 wire [5:0] med_out;
	 wire [5:0] small_out;
	 wire [5:0] bg_out;
	 wire [1:0] selEntity;
	 
	 assign selEntity = {entity,draw_mode}; 
	 
	
	always@(*) begin
			if (selEntity[0] == 1)
				mem_out <= bg_out;
			else begin
				if (selEntity[1] == 0)
					mem_out <= fire_out;
				else 
					mem_out <= 6'b000011; //BLUE
			end
	end
	
	 
	always@(*) begin
		if (selSize == 0)
			fire_out = small_out;
		else if (selSize == 1)
			fire_out = med_out;
		else if (selSize == 2)
			fire_out = big_out;
		else
			fire_out = small_out; 
	end
	 
	always@(posedge clk) begin
		if (selEntity[1] == 1) begin
			xOffset <= xIn;
			yOffset <= 115;
			yBound <= 4;
			xBound <= 4;
		end
		else if (selEntity[1] == 0) begin 
			yOffset <= 0;
			yBound <= 79;
			xBound <= 39;
			if(selectPos == 0)
				xOffset <= 20;
			else if (selectPos == 1)
						xOffset <= 70;
			else if (selectPos == 2)
						  xOffset <= 115;
			else 
				xOffset <= 0; // COULD OPTIMIZE BY JUST HAVING ELSE OFFSET = 115
		end
		else begin
			yOffset <= 0;
			xOffset <= 0;
			yBound <= 0;
			xBound <= 0;
		end
	end

    // Register logic
    always@(posedge clk) begin
        if(!resetn) begin 
		outCol <= 5'b0;
		outX <= 7'b0;
		outY <= 6'b0;
        end
        else begin
		if (ld_x)
			outX <= countX;
		if (ld_y)
			outY <= countY;
	     end
	if (ld_col)
		if (mem_out == 6'b111111)
			outCol <= bg_out;
		else
			outCol <= mem_out;
    end

	// The counter for x
	always @ (posedge clk) begin
		if (!resetn)
			countX <= 0;
		else if (set_x)
			countX <= xOffset;
		else if (countX == (xBound + xOffset))
			countX <= xOffset;
		else if (increment)
			countX <= countX + 1;
	end

	// The counter for y
	always @ (posedge clk) begin
		if (!resetn) begin
			countY <= 0;
			done <= 0;
		end
		else if (set_y) begin
			countY <= yOffset;
			done <= 0;
		end
		else if ((countX == (xBound + xOffset)) && (countY == yBound + yOffset)) begin
			countY <= 0;
			done <= 1;
			end
		else if (countX == (xBound + xOffset)) begin
			countY <= countY + 1;
			done <= 0;
		end
	end
	
	always @ (posedge clk) begin
	
	end

	bigfire b0 (
	   .clock(clk),
	   .q(big_out),
	   .wren(1'b0),
	   .address((countY * 40) + (countX - xOffset)),
	   .data(6'b000000)	
	);
	
	medfire b1 (
	   .clock(clk),
	   .q(med_out),
	   .wren(1'b0),
	   .address((countY * 40) + (countX - xOffset)),
	   .data(6'b000000)	
	);
	
	smallfire b2 (
	   .clock(clk),
	   .q(small_out),
	   .wren(1'b0),
	   .address((countY * 40) + (countX - xOffset)),
	   .data(6'b000000)	
	);
	
	background1 bg1 (
			.clock(clk),
			.q(bg_out),
			.wren(1'b0),
			.address((countY * 160) + (countX)),
			.data(6'b000000)
		);
endmodule

